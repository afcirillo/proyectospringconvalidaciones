<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>     <!-- Debemos importar "form" -->

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulario de Registro</title>
</head>
<body>

<form:form action="procesarFormulario" modelAttribute="elAlumno">	<!-- La info la mandaremos a procesarFormulario -->

Nombre: <form:input path="nombre"/>	<form:errors path="nombre" style="color:red"/>
<!-- Aqui se llaman a los getters de Alumno, por ende  utiliza su nombre -->

<br><br><br>

Apellido: <form:input path="apellido"/>	<form:errors path="apellido" style="color:red"/>
<!-- Aqui se llaman a los getters de Alumno, por ende  utiliza su nombre -->

<br><br><br>

Edad: <form:input path="edad"/>	<form:errors path="edad" style="color:red"/>
<!-- Aqui se llaman a los getters de Alumno, por ende  utiliza su nombre -->

<br><br><br>

Correo Electr�nico: <form:input path="email"/>	<form:errors path="email" style="color:red"/>
<!-- Aqui se llaman a los getters de Alumno, por ende  utiliza su nombre -->

<br><br><br>

C�digo Postal: <form:input path="codigoPostal"/>	<form:errors path="codigoPostal" style="color:red"/>
<!-- Aqui se llaman a los getters de Alumno, por ende  utiliza su nombre -->

<br><br><br>

Asignaturas Optativas:<br/>

<form:select path="optativa" multiple="true">	<!-- Aqui ponemos opcionales -->

	<form:option value="Dise�o" label="Dise�o"/>
	<form:option value="Aikido" label="Aikido"/>
	<form:option value="Comercio" label="Comercio"/>
	<form:option value="Gimnasia" label="Gimnasia"/>
	
</form:select>

<br/><br/><br/>

	Bella Vista<form:radiobutton path="ciudadEstudios" value="Bella Vista"/>	<!-- Aqui ponemos radio buttons -->
	San Miguel<form:radiobutton path="ciudadEstudios" value="San Miguel"/>

<br/><br/><br/>
			
	Ingl�s<form:checkbox path="idiomasAlumno" value="Ingl�s"/>		<!-- Aqui ponemos checkboxes -->
	Franc�s<form:checkbox path="idiomasAlumno" value="Franc�s"/>
	Italiano<form:checkbox path="idiomasAlumno" value="Italiano"/>
	Alem�n<form:checkbox path="idiomasAlumno" value="Alem�n"/>
	
<br/><br/><br/>

<input type="submit"  value="Enviar">

</form:form>


</body>
</html>