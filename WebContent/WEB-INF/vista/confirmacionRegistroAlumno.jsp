<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Confirmaci�n de Registro</title>
</head>
<body>

El alumno <strong>${elAlumno.nombre}</strong> <strong>${elAlumno.apellido}</strong> , de <strong>${elAlumno.edad}</strong> a�os de edad,
 correo <strong>${elAlumno.email}</strong> y c�digo postal <strong>${elAlumno.codigoPostal}</strong>, se ha registrado con �xito.
<br/>
La asignatura escogida es <strong>${elAlumno.optativa}</strong>.
<br/>
La ciudad donde iniciar� los estudios el alumno es <strong>${elAlumno.ciudadEstudios}</strong>.
<br/>
Los idiomas escogidos por el alumno son: <strong>${elAlumno.idiomasAlumno}</strong>.

</body>
</html>