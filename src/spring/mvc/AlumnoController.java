package spring.mvc;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/alumno")
public class AlumnoController {	//Este es el controlador del registro
	
	@InitBinder
	public void miBinder(WebDataBinder binder) {
		
		StringTrimmerEditor recortaEspaciosBlanco=new StringTrimmerEditor(true);
		
		binder.registerCustomEditor(String.class, recortaEspaciosBlanco);
	}
	
	
	@RequestMapping("/muestraFormulario")
	public String muestrarFormulario(Model modelo) {//aqui guardaremos el objeto que queremos que viaje 
													//entre el formulario registro y la vista confirmacion
		Alumno elAlumno=new Alumno();
		
		modelo.addAttribute("elAlumno", elAlumno);
		
		return "alumnoRegistroFormulario";
		
	}
	
	@RequestMapping("/procesarFormulario")
	public String procesarFormulario(@Valid @ModelAttribute("elAlumno") Alumno alumno, BindingResult resultadoValidacion) {	//aqui se va a guardar el alumno que viaja en el Model
		
		if(resultadoValidacion.hasErrors()) {
			return "alumnoRegistroFormulario";
		}else {
			return "confirmacionRegistroAlumno";
		}
		
	}

}
