package spring.mvc.validaiconespersonalizadas;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Constraint(validatedBy = CPostalBellaVistaValidacion.class)//vid44 idnica futura clase que contendra la logica de la validaicon
@Target( { ElementType.METHOD, ElementType.FIELD })//destino de nuestra validacion a metodos o campos
@Retention(RetentionPolicy.RUNTIME)//chequea la anotiacion en tiempo de ejecucion
public @interface CPostalBellaVista {
	
	public String value() default "16";
	public String message() default "El codigo postal debe comenzar con 16";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
