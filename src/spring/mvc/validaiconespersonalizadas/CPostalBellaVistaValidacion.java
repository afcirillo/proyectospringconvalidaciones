package spring.mvc.validaiconespersonalizadas;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CPostalBellaVistaValidacion implements ConstraintValidator<CPostalBellaVista, String> {

	@Override
	public void initialize(CPostalBellaVista elCodigo) {
		prefijoCodigoBellaVista=elCodigo.value();
	}

	@Override
	public boolean isValid(String arg0,ConstraintValidatorContext arg1) {
		boolean valCodigo;
		if(arg0!=null) valCodigo=arg0.startsWith(prefijoCodigoBellaVista);
		else  return valCodigo=true;
		return valCodigo;
	}

	private String prefijoCodigoBellaVista;
}


