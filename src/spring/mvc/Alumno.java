package spring.mvc;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import spring.mvc.validaiconespersonalizadas.CPostalBellaVista;

public class Alumno {	//aqui creo mi alumno con los datos a llenar
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	

	public String getOptativa() {
		return optativa;
	}

	public void setOptativa(String optativa) {
		this.optativa = optativa;
	}


	public String getCiudadEstudios() {
		return ciudadEstudios;
	}

	public void setCiudadEstudios(String ciudadEstudios) {
		this.ciudadEstudios = ciudadEstudios;
	}


	public String getIdiomasAlumno() {
		return idiomasAlumno;
	}

	public void setIdiomasAlumno(String idiomasAlumno) {
		this.idiomasAlumno = idiomasAlumno;
	}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}



	@NotNull
	@Size(min=2, message=" *Campo requerido.")
	private String nombre;
	
	@NotNull
	@Size(min=2, message=" *Campo requerido.")
	private String apellido;
	
	@Email
	private String email;
	
	@Min(value=10, message=" *No se permiten menores de 10 a�os.")
	@Max(value=100, message=" *No se permiten mayores de 100 a�os.")
	private int edad;
	
	//@Pattern(regexp="[0-9]{4}", message=" *Solo 4 valores num�ricos.")// "^(16)[0-9]{2}" para simplificar todo lo del vid 44/45
	@CPostalBellaVista
	private String codigoPostal;
	
	private String optativa;
	
	private String ciudadEstudios;
	
	private String idiomasAlumno;

}
