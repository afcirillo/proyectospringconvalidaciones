package spring.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/principal")
public class HolaAlumnosControlador {
	
	@RequestMapping("/muestraFormulario")	//vid28 Anotacion que permite pedir una peticion de una URL
	public String muestraFormulario() {	//Metodo para pedirle el formulario al servidor. proporciona el formulario
		
		return "HolaAlumnosFormulario";
	}
	
	@RequestMapping("/procesarFormulario")//Metodo para que procese el formulario que hemos enviado
	public String procesarFormulario() {
		
		return "HolaAlumnosSpring";
	}
	
	@RequestMapping("/procesarFormulario2")
	//public String otroProcesoFormulario(HttpServletRequest request, Model modelo) {
	public String otroProcesoFormulario(@RequestParam("nombreAlumno")String nombre, Model modelo) {//vid32 anotation que reemplaza y simplifica codigo

		//String nombre=request.getParameter("nombreAlumno");
		
		nombre+=" es el mejor alumno.";
		
		String mensajeFinal="Quien es el mejor alumno? " + nombre;
		
		//agregando info al modelo
		modelo.addAttribute("mensajeClaro", mensajeFinal);
		
		return "HolaAlumnosSpring";
	}
}
